const path       = require('path');
const express    = require('express');
const bodyParser = require('body-parser');
const fallback   = require('express-history-api-fallback');

const router     = require('./router.js');


const app        = express();
const publicPath = path.join(__dirname, 'client', 'build');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(publicPath));
app.use('/images', express.static(path.join(__dirname, 'client', 'public', 'images')));
app.use(router);
app.use(fallback('index.html', { root: publicPath }));
app.set('port', process.env.PORT || 45678);


app.listen(app.get('port'), function(){
  console.log('Server http://localhost:' + app.get('port'))
})