import React, { Component } from 'react'
import axios from 'axios'

import './search.scss'

import Input from '../../components/Input/Input'
import Issues from '../../components/Issues/Issues'
import RepositoriesBox from '../../components/RepositoriesBox/RepositoriesBox'
import Pagination from '../../components/Pagination/Pagination'

import { formatFilters, makeLinks } from '../../utils'

export default class Search extends Component {
  state = {
    filters: '',

    repository: '',
    repositories: [],
    activeRepositoryPage: 0,
    repositoriesLinks: [],
    activeRepository: undefined,

    issues: [],
    issuesLinks: [],
    activeIssuePage: 0,

    error: undefined,
    onLoad: false,
    onLoadIssues: false
  }

  clearPullRequests = (allIssues) => {
    return allIssues.filter((issue) => {
      if (!issue.pull_request) return issue;
    })
  }

  setFilters = (filters) => {
    this.setState({filters: filters})
  }

  setRepository = (repository) => {
    this.setState({repository: repository})
  }

  onSearch = () => {
    const { repository } = this.state;

    if (repository === '') return this.setState({error: 'Введите репозиторий'})
    
    let url = `https://api.github.com/search/repositories?q=${repository}+in:name`

    const formattedFilters = this.state.filters !== '' ? formatFilters(this.state.filters) : undefined;

    if (formattedFilters) url = `https://api.github.com/search/repositories?q=${repository}+in:name${formattedFilters}`

    this.setState({
      onLoad: true,
      repositories: [],
      issues: [],
      repositoriesLinks:[],
      issuesLinks:[],
      activeRepository: undefined
    });

    axios.get(url, {
      headers: {
        "Accept": "application/vnd.github.VERSION.text+json"
      }
    })
    .then(response => {
      const repos = response.data.items;
      if (repos.length < 1) return this.state({error: 'Такого репозитория нет', onLoad: false});

      const links = makeLinks(response.headers.link);

      this.setState({repositories: repos, onLoad: false, repositoriesLinks: links})
    })
    .catch(err => {
      this.setState({onLoad: false, error: 'Такого репозитория нет'})
      console.log('Error ', err)
    })
  }

  requestIssues = () => {
    const fullname = this.state.activeRepository.full_name;
    this.setState({onLoadIssues: true}, () => {
      axios.get(`https://api.github.com/repos/${fullname}/issues`, {
        headers: {
          "Accept": "application/vnd.github.VERSION.text+json"
        }
      })
      .then(response => {        
        if (response.data.length < 1) return this.setState({issues: [], onLoadIssues: false})

        if (response.headers.link) {
          const links = makeLinks(response.headers.link);
          return this.setState({issues: this.clearPullRequests(response.data), onLoadIssues: false, issuesLinks: links})
        }

        this.setState({issues: this.clearPullRequests(response.data), onLoadIssues: false, issuesLinks: []})
      })
      .catch(err => {
        this.setState({onLoadIssues: false, error: 'Ошибка поиска issues', issues: []});
        console.error('Error ', err)
      })
    })
  }

  setActiveRepository = (repository) => {
    this.setState({activeRepository: repository, issues:[], issuesLinks: [], error: undefined}, this.requestIssues);
  }

  openIssuesByLink = (link) => {
    this.setState({onLoadIssues: true})

    axios.get(link, {headers: {"Accept": "application/vnd.github.VERSION.text+json"}})
    .then(response => {
      this.setState({issues: this.clearPullRequests(response.data), onLoadIssues: false})
    })
    .catch(err => {
      this.setState({onLoadIssues: false, error: 'Ошибка поиска issues', issues: []})
      console.log('Error ', err)
    })
  }

  openRepoByLink = (link) => {
    this.setState({onLoad: true})

    axios.get(link, {headers: {"Accept": "application/vnd.github.VERSION.text+json"}})
    .then(response => {
      const repos = response.data.items;
      if (repos.length < 1) return this.state({error: 'Такого репозитория нет', onLoad: false});

      this.setState({repositories: repos, onLoad: false})
    })
    .catch(err => {
      this.setState({onLoad: false, error: 'Такого репозитория нет'})
      console.log('Error ', err)
    })
  }

  setActiveRepositoryPage = (index) => {
    this.setState({activeRepositoryPage: index})
  }
  
  setActiveIssuePage = (index) => {
    this.setState({activeIssuePage: index})
  }

  resetError = () => this.setState({error: undefined})

  render() {
    return (
      <div className="search">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <Input onSearch={this.onSearch}
            placeholder={'Введите репозиторий'}
            setRepository={this.setRepository}
            search={true} 
            resetError={this.resetError}
            error={this.state.error}
          />
          <span className="form__text">Язык </span>
          <Input onSearch={this.onSearch}
            setFilters={this.setFilters}
            placeholder={'php, javascript, C'}
          />
          {this.state.error ? <div className="error">{this.state.error}</div> : null}
          {this.state.onLoad ? <div>Загрузка...</div> : null}
        </form>
        {this.state.repositories ?
          <RepositoriesBox repositories={this.state.repositories}
            setActiveRepository={this.setActiveRepository}
            activeRepository={this.state.activeRepository}
          />
          :
          null
        }
        <Pagination source={this.state.repositoriesLinks}
          openRepoByLink={this.openRepoByLink}
          active={this.state.activeRepositoryPage}
          setActive={this.setActiveRepositoryPage}
        />
        <Issues
          issues={this.state.issues}
          onLoad={this.state.onLoadIssues}
          repository={this.state.activeRepository}
        />
        <Pagination source={this.state.issuesLinks}
          openIssuesByLink={this.openIssuesByLink}
          active={this.state.activeIssuePage}
          setActive={this.setActiveIssuePage}
        />
      </div>
    )
  }
}