import React, { Component } from 'react'
import axios from 'axios'

import './issueItem.scss'
import { formatDate } from '../../utils'
 
import Comment from '../../components/Comment/Comment'

export default class IssueItem extends Component {
  state = {
    isHide: true,
    comments: undefined,
    isShowComments: false,
    onLoad: false
  }

  onHide = (e) => {
    if (e.target === this.link) return;

    this.setState({isHide: !this.state.isHide})
  }

  hideComments = () => {
    this.setState({isShowComments: false})
  }

  openComments = () => {
    if (this.state.comments) return this.setState({isShowComments: true});

    this.setState({onLoad: true})
    axios.get(this.props.issue.comments_url, {headers: {"Accept": "application/vnd.github.VERSION.text+json"}})
    .then(response => {
      this.setState({comments: response.data, onLoad: false, isShowComments: true})
    })
    .catch(err => {
      this.setState({onLoad: false, comment: undefined, isShowComments: false})
      console.log('Error ', err)
    })
  }

  renderComments = () => {
    return this.state.comments.map((comment, i) => 
      <Comment comment={comment} key={i} />
    )
  }

  renderCommentButton = () => {
    const { issue } = this.props;

    if (this.state.onLoad) return <div className="issue-item__link">Загрузка...</div>

    if (this.state.isShowComments)
      return <div className="issue-item__link" onClick={this.hideComments}>Скрыть комментарии</div> 

    if (issue.comments > 0) return <div className="issue-item__link" onClick={this.openComments}>Комментарии {issue.comments}</div>

    return <div className="issue-item__link">Нет комментариев</div>
  }

  render() {
    const { issue } = this.props;
    return (
      <div className={this.state.isHide ? "issue-item" : "issue-item _active"}>
        <div className="issue-item__top">
          <div className="issue-item__title" onClick={this.onHide}>{issue.title}</div>
          <a className="issue-item__author"
            ref={(link) => this.link = link}
            href={issue.user.html_url}>
            {issue.user.login}
          </a>
        </div>
        <div className="issue-item__bottom">
          <div className="issue-item__number">number: #{issue.number}</div>
          <div className="issue-item__number">{formatDate(issue.created_at)}</div>
        </div>
        
        {this.state.isHide ? 
          null
          :
          <div className="issue-item__hidden">
            <div className="issue-item__text">
              {issue.body_text}
              {this.renderCommentButton()}
            </div>
            {this.state.comments && this.state.isShowComments ? 
              <div className="issue-item__comments-box">
                {this.renderComments()}
              </div>
              :
              null
            }
          </div>
        }
      </div>
    )
  }
}
