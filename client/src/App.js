import React, { Component } from 'react'
import { Router } from 'react-router-dom'
import createHistory from "history/createBrowserHistory"
import './App.scss';

import Header from './components/Header/Header'
import Search from './containers/Search/Search'

const history = createHistory();

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <div className="wrapper">
          <Header logo={"Issues"} />
          <div className="main-content">
            <div className="inner">
              <Search />
            </div>
          </div>
        </div>
      </Router>
    );
  }
}
export default App;