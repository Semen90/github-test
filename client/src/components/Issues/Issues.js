import React, { Component } from 'react'

import IssueItem from '../../containers/IssueItem/IssueItem'
import './issues.scss'

export default class Issues extends Component {
  renderIssues = () => {
    if (this.props.onLoad) return <div className="empty _tac">Загрузка...</div>;

    if (this.props.repository === undefined) return <div className="empty _tac">Укажите репозитрой</div>;

    if (this.props.issues.length < 1) return <div className="empty _tac">Нет issues</div>

    return this.renderListIssues()
  }

  renderListIssues = () => {
    return this.props.issues.map((issue, i) => <IssueItem issue={issue} key={i}/>)
  }

  render() {
    return (
      <div className="issues">
        <div className="title _tac">Issues</div>
        <div className="spacer _big _t10"></div>
        <div className="issues__box">{this.renderIssues()}</div>
      </div>
    )
  }
}
