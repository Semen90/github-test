import React, { Component } from 'react'

import './header.scss'

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="inner">
          <div className="logo">{this.props.logo}</div>
        </div>
      </div>
    )
  }
}
