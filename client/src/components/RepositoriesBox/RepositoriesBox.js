import React, { Component } from 'react'

import Repository from '../Repository/Repository'

export default class RepositoriesBox extends Component {
  
  renderRepositories = () => {
    return this.props.repositories.map((repo, i) => (
      <Repository repository={repo} key={i}
        setActiveRepository={this.props.setActiveRepository}
        activeRepository={this.props.activeRepository} />
    ))
  }

  render() {
    return (
      <div className="repositories-box">
        {this.renderRepositories()}
      </div>
    )
  }
}
