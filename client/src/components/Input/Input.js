import React, { Component } from 'react'

import './input.scss'

export default class Input extends Component {
  state = {
    value: ''  
  }

  onSearch = () => {
    this.props.onSearch()
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.props.onSearch()
    }
  }

  onChange = (e) => {
    if (this.props.setFilters) {
      this.setState({value: e.target.value})
      return this.props.setFilters(e.target.value);
    } 

    if (this.props.error) this.props.resetError();

    this.setState({value: e.target.value})
    return this.props.setRepository(e.target.value)
  }

  render() {
    return (
      <div className="input">
        <input type="text"
          className="_text"
          placeholder={this.props.placeholder}
          value={this.state.value}
          onChange={this.onChange}
          onKeyPress={this.handleKeyPress}
        />
        {this.props.search ? <span className="icon _search" onClick={this.onSearch}></span> : null}        
      </div>
    )
  }
}
