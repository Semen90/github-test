import React from 'react'
import PropTypes from 'prop-types'

import './comment.scss'
import { formatDate } from '../../utils'

const Comment = (props) => {

  const { comment } = props;
  
  return (
    <div className="comment">
      <a className="comment__top" href={comment.user.html_url}>
        <img className="comment__avatar" src={comment.user.avatar_url} alt={comment.user.login} />          
        <span className="comment__login">{comment.user.login}</span>        
      </a>
      <div>{comment.body_text}</div>
      <div className="comment__date">{formatDate(comment.created_at)}</div>
    </div>
  )
}

Comment.propTypes = {
  comment: PropTypes.object
}

export default Comment;
