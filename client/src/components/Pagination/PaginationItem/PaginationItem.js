import React from 'react';
import urlParse from 'url-parse'

const PaginationItem = (props) => {

  const { link } = props;

  const getNumber = (link) => {
    return urlParse(link, true).query.page
  }

  const sendLink = (e) => {
    e.preventDefault();
    props.setActive(props.index)
    if (props.openIssuesByLink) return (props.openIssuesByLink(e.target.href));
    return props.openRepoByLink(e.target.href);
  }

  return (
    <li className={props.active ? "_active" : ""} onClick={sendLink}>
      <a href={link}>{getNumber(link)}</a>
    </li>
  );
}

export default PaginationItem;