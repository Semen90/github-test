import React, { Component } from 'react'

import './pagination.scss'

import PaginationItem from './PaginationItem/PaginationItem'

export default class Pagination extends Component {

  setActive = (index) => {
    this.props.setActive(index)
  }

  renderPagination = () => {
    return this.props.source.map((link, i) =>
      <PaginationItem link={link} key={i} index={i}
        active={this.props.active === i}
        setActive={this.setActive}
        openIssuesByLink={this.props.openIssuesByLink}
        openRepoByLink={this.props.openRepoByLink}
      />
    )
  }

  render() {
    return (
      <ul className="pagination">
        {this.renderPagination()}
      </ul>
    )
  }
}
