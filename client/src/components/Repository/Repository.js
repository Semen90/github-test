import React, { Component } from 'react'

import './repository.scss'

export default class Repository extends Component {

  setActiveRepository = () => {
    this.props.setActiveRepository(this.props.repository)
  }

  render() {
    const { repository, activeRepository } = this.props;

    let active = false;
    if (activeRepository && activeRepository.id === this.props.repository.id) active = true; 

    return (
      <div className={active ? "repository _active" : "repository"} onClick={this.setActiveRepository}>
        <div className="repository__name">{repository.name}</div>
        <div className="repository__fullname">{repository.full_name}</div>
      </div>
    )
  }
}
