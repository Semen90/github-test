import urlParse from 'url-parse'

export const formatDate = (date) => {
  const oldDate = new Date(date);
  const year = oldDate.getFullYear()
  const month = oldDate.getMonth() + 1;
  const day = oldDate.getDate()
  return `${day}.${month}.${year}`
}

export const formatFilters = (filters) => {
  let arr = filters.split(', ');
  return arr.map(item => '+language:'+ item).join('')
}


export const makeLinks = (linksOneString) => {
  let links = linksOneString.split(', ');
  let lastLink = links[1].split('; ')[0]
  lastLink = lastLink.substring(1, lastLink.length - 1)
  return makeListLinks(lastLink)
}

function makeListLinks(lastLink) {
  let parsed = urlParse(lastLink, true)
  const lastIndex = parsed.query.page;
  let linksList = [];

  for (let i = 1; i <= lastIndex; i++) {
    let prsd = function(page) {
      parsed.query.page = page;
      return parsed;
    }(i)
    linksList.push(prsd.toString());
  }
  return linksList;
}