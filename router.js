var router     = require('express').Router();
var path       = require('path');

//
// ─── INDEX ──────────────────────────────────────────────────────────────────────
//  
router.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
});


//
// ─── OTHERS ──────────────────────────────────────────────────────────────────────
// 

module.exports = router;